<?php

use Illuminate\Database\Seeder;

class ShopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Shop::class, 20)->create()->each(function($u) {
            $u->products()->saveMany(factory(App\Product::class,5)->make());
        });
    }
}
