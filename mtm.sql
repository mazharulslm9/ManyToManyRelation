-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 24, 2016 at 09:16 AM
-- Server version: 10.1.9-MariaDB-log
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mtm`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_05_23_161837_create_products_table', 1),
('2016_05_23_161844_create_shops_table', 1),
('2016_05_23_170255_product_shop', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Rossie Hamill', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(2, 'Melvina Sawayn', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(3, 'Mr. Issac Fay I', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(4, 'Berneice Quigley', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(5, 'Alfonso Russel II', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(6, 'Nedra Stokes PhD', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(7, 'Ward Mayert', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(8, 'Claud Cassin', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(9, 'Florida Lebsack III', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(10, 'Lawrence Considine', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(11, 'Dr. Justice Bayer III', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(12, 'Laney Gerhold V', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(13, 'Kendrick Considine', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(14, 'Mark Terry', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(15, 'Miss Araceli Ullrich', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(16, 'Dr. Issac Welch', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(17, 'Jannie Schumm Sr.', '2016-05-23 12:02:09', '2016-05-23 12:02:09'),
(18, 'Mrs. Kathleen Daniel DVM', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(19, 'Antonina Lebsack', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(20, 'Marion Breitenberg', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(21, 'Nolan Johns', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(22, 'Javonte Macejkovic', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(23, 'Lucy Kerluke', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(24, 'Loyal Kohler', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(25, 'Kayley Hegmann', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(26, 'Nelson Deckow Sr.', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(27, 'Gunnar Sauer', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(28, 'Lambert Schumm', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(29, 'Ms. Nina Lakin IV', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(30, 'Dr. Patsy Brakus', '2016-05-23 12:02:10', '2016-05-23 12:02:10'),
(31, 'Nicholas Dare MD', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(32, 'Osbaldo Mitchell', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(33, 'Cristal Kertzmann', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(34, 'Kari Murazik', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(35, 'Kelton Kovacek', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(36, 'Verner Smitham', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(37, 'Luisa Dietrich', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(38, 'Pansy Schowalter', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(39, 'Krystina Okuneva', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(40, 'Jarrett Stamm IV', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(41, 'Braden Feeney Jr.', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(42, 'Mr. Carlos Schmitt DDS', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(43, 'Miss Allene Zemlak', '2016-05-23 12:02:11', '2016-05-23 12:02:11'),
(44, 'Woodrow Rogahn Sr.', '2016-05-23 12:02:12', '2016-05-23 12:02:12'),
(45, 'Hassie Schowalter', '2016-05-23 12:02:12', '2016-05-23 12:02:12'),
(46, 'Landen Goldner', '2016-05-23 12:02:12', '2016-05-23 12:02:12'),
(47, 'Dr. Stewart Stracke', '2016-05-23 12:02:12', '2016-05-23 12:02:12'),
(48, 'Kiara Cremin', '2016-05-23 12:02:12', '2016-05-23 12:02:12'),
(49, 'Elisabeth Jakubowski', '2016-05-23 12:02:12', '2016-05-23 12:02:12'),
(50, 'Dr. Jayne Satterfield DVM', '2016-05-23 12:02:12', '2016-05-23 12:02:12'),
(51, 'Ms. Marisa Hoppe', '2016-05-23 12:02:12', '2016-05-23 12:02:12'),
(52, 'Shyann Murphy', '2016-05-23 12:02:12', '2016-05-23 12:02:12'),
(53, 'Mr. Unique Batz III', '2016-05-23 12:02:12', '2016-05-23 12:02:12'),
(54, 'Rickey McGlynn', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(55, 'Thora Conn', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(56, 'Breanne Muller', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(57, 'Karina Smith', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(58, 'Holly Mayert Jr.', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(59, 'Mrs. Alysha Dickens', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(60, 'Elissa Ziemann PhD', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(61, 'Dr. Porter Schowalter', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(62, 'Winfield Olson', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(63, 'Godfrey Reynolds', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(64, 'Dandre Bogan', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(65, 'Prof. Magnus Herman', '2016-05-23 12:02:13', '2016-05-23 12:02:13'),
(66, 'Loy Emard', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(67, 'Marvin Wunsch', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(68, 'Stephon Huel', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(69, 'Jadon Brakus', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(70, 'Madaline Corkery', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(71, 'Fernando Krajcik', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(72, 'Elsie Swift', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(73, 'Miss Bette Emmerich', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(74, 'Mrs. Candice McKenzie', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(75, 'Willie Hagenes', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(76, 'Michael Lockman', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(77, 'Ali Cassin', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(78, 'Erwin Rau', '2016-05-23 12:02:14', '2016-05-23 12:02:14'),
(79, 'Prof. Lucie Shields', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(80, 'Payton Windler', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(81, 'Cordia Zemlak', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(82, 'Murray Glover', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(83, 'Carson Murazik V', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(84, 'Ms. Maybelle Rowe DVM', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(85, 'Kayden Collier', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(86, 'Jimmie Gulgowski', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(87, 'Valentine Skiles DVM', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(88, 'Cierra Gislason V', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(89, 'Eduardo Williamson', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(90, 'Novella Lindgren', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(91, 'Rosetta Gulgowski', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(92, 'Domenick Barton', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(93, 'Dr. Bria Bednar', '2016-05-23 12:02:15', '2016-05-23 12:02:15'),
(94, 'Breanne Vandervort', '2016-05-23 12:02:16', '2016-05-23 12:02:16'),
(95, 'Alexanne Considine MD', '2016-05-23 12:02:16', '2016-05-23 12:02:16'),
(96, 'Prof. Reba Gusikowski IV', '2016-05-23 12:02:16', '2016-05-23 12:02:16'),
(97, 'Brando Runte', '2016-05-23 12:02:16', '2016-05-23 12:02:16'),
(98, 'Eva Walsh', '2016-05-23 12:02:16', '2016-05-23 12:02:16'),
(99, 'Vaughn Bernier', '2016-05-23 12:02:16', '2016-05-23 12:02:16'),
(100, 'Arnulfo Von', '2016-05-23 12:02:16', '2016-05-23 12:02:16'),
(101, 'Mr. Friedrich Wolff', '2016-05-23 12:02:19', '2016-05-23 12:02:19'),
(102, 'Evalyn Marks', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(103, 'Prof. Fredy DuBuque I', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(104, 'Madison O\'Conner', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(105, 'Prof. Edwardo Pollich I', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(106, 'Xavier Kohler DDS', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(107, 'Marisa Quigley', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(108, 'Dalton Leffler', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(109, 'Vinnie Weissnat IV', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(110, 'Florine Deckow', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(111, 'Teresa Simonis', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(112, 'Wilfredo Beier', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(113, 'Jordi Gorczany', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(114, 'Pierre Murray', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(115, 'Rod VonRueden', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(116, 'Chadd Konopelski', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(117, 'Ramona Klocko', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(118, 'Anya Luettgen', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(119, 'Eula Pacocha', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(120, 'Pansy McGlynn', '2016-05-23 12:02:20', '2016-05-23 12:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `product_shop`
--

CREATE TABLE `product_shop` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_shop`
--

INSERT INTO `product_shop` (`id`, `product_id`, `shop_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 1, NULL, NULL),
(4, 4, 1, NULL, NULL),
(5, 5, 1, NULL, NULL),
(6, 6, 2, NULL, NULL),
(7, 7, 2, NULL, NULL),
(8, 8, 2, NULL, NULL),
(9, 9, 2, NULL, NULL),
(10, 10, 2, NULL, NULL),
(11, 11, 3, NULL, NULL),
(12, 12, 3, NULL, NULL),
(13, 13, 3, NULL, NULL),
(14, 14, 3, NULL, NULL),
(15, 15, 3, NULL, NULL),
(16, 16, 4, NULL, NULL),
(17, 17, 4, NULL, NULL),
(18, 18, 4, NULL, NULL),
(19, 19, 4, NULL, NULL),
(20, 20, 4, NULL, NULL),
(21, 21, 5, NULL, NULL),
(22, 22, 5, NULL, NULL),
(23, 23, 5, NULL, NULL),
(24, 24, 5, NULL, NULL),
(25, 25, 5, NULL, NULL),
(26, 26, 6, NULL, NULL),
(27, 27, 6, NULL, NULL),
(28, 28, 6, NULL, NULL),
(29, 29, 6, NULL, NULL),
(30, 30, 6, NULL, NULL),
(31, 31, 7, NULL, NULL),
(32, 32, 7, NULL, NULL),
(33, 33, 7, NULL, NULL),
(34, 34, 7, NULL, NULL),
(35, 35, 7, NULL, NULL),
(36, 36, 8, NULL, NULL),
(37, 37, 8, NULL, NULL),
(38, 38, 8, NULL, NULL),
(39, 39, 8, NULL, NULL),
(40, 40, 8, NULL, NULL),
(41, 41, 9, NULL, NULL),
(42, 42, 9, NULL, NULL),
(43, 43, 9, NULL, NULL),
(44, 44, 9, NULL, NULL),
(45, 45, 9, NULL, NULL),
(46, 46, 10, NULL, NULL),
(47, 47, 10, NULL, NULL),
(48, 48, 10, NULL, NULL),
(49, 49, 10, NULL, NULL),
(50, 50, 10, NULL, NULL),
(51, 51, 11, NULL, NULL),
(52, 52, 11, NULL, NULL),
(53, 53, 11, NULL, NULL),
(54, 54, 11, NULL, NULL),
(55, 55, 11, NULL, NULL),
(56, 56, 12, NULL, NULL),
(57, 57, 12, NULL, NULL),
(58, 58, 12, NULL, NULL),
(59, 59, 12, NULL, NULL),
(60, 60, 12, NULL, NULL),
(61, 61, 13, NULL, NULL),
(62, 62, 13, NULL, NULL),
(63, 63, 13, NULL, NULL),
(64, 64, 13, NULL, NULL),
(65, 65, 13, NULL, NULL),
(66, 66, 14, NULL, NULL),
(67, 67, 14, NULL, NULL),
(68, 68, 14, NULL, NULL),
(69, 69, 14, NULL, NULL),
(70, 70, 14, NULL, NULL),
(71, 71, 15, NULL, NULL),
(72, 72, 15, NULL, NULL),
(73, 73, 15, NULL, NULL),
(74, 74, 15, NULL, NULL),
(75, 75, 15, NULL, NULL),
(76, 76, 16, NULL, NULL),
(77, 77, 16, NULL, NULL),
(78, 78, 16, NULL, NULL),
(79, 79, 16, NULL, NULL),
(80, 80, 16, NULL, NULL),
(81, 81, 17, NULL, NULL),
(82, 82, 17, NULL, NULL),
(83, 83, 17, NULL, NULL),
(84, 84, 17, NULL, NULL),
(85, 85, 17, NULL, NULL),
(86, 86, 18, NULL, NULL),
(87, 87, 18, NULL, NULL),
(88, 88, 18, NULL, NULL),
(89, 89, 18, NULL, NULL),
(90, 90, 18, NULL, NULL),
(91, 91, 19, NULL, NULL),
(92, 92, 19, NULL, NULL),
(93, 93, 19, NULL, NULL),
(94, 94, 19, NULL, NULL),
(95, 95, 19, NULL, NULL),
(96, 96, 20, NULL, NULL),
(97, 97, 20, NULL, NULL),
(98, 98, 20, NULL, NULL),
(99, 99, 20, NULL, NULL),
(100, 100, 20, NULL, NULL),
(101, 101, 21, NULL, NULL),
(102, 102, 22, NULL, NULL),
(103, 103, 23, NULL, NULL),
(104, 104, 24, NULL, NULL),
(105, 105, 25, NULL, NULL),
(106, 106, 26, NULL, NULL),
(107, 107, 27, NULL, NULL),
(108, 108, 28, NULL, NULL),
(109, 109, 29, NULL, NULL),
(110, 110, 30, NULL, NULL),
(111, 111, 31, NULL, NULL),
(112, 112, 32, NULL, NULL),
(113, 113, 33, NULL, NULL),
(114, 114, 34, NULL, NULL),
(115, 115, 35, NULL, NULL),
(116, 116, 36, NULL, NULL),
(117, 117, 37, NULL, NULL),
(118, 118, 38, NULL, NULL),
(119, 119, 39, NULL, NULL),
(120, 120, 40, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Kenna Stanton', '2016-05-23 12:02:07', '2016-05-23 12:02:07'),
(2, 'Prof. Dolly Treutel', '2016-05-23 12:02:07', '2016-05-23 12:02:07'),
(3, 'Nichole Glover', '2016-05-23 12:02:07', '2016-05-23 12:02:07'),
(4, 'Prof. Madelynn Greenholt Jr.', '2016-05-23 12:02:07', '2016-05-23 12:02:07'),
(5, 'Miss Tiana Stanton DVM', '2016-05-23 12:02:07', '2016-05-23 12:02:07'),
(6, 'Dr. Aida Zboncak Sr.', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(7, 'Kristoffer Yundt', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(8, 'Dr. Morris Jacobs', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(9, 'Dr. Edythe Kunze', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(10, 'Prof. Broderick Feest DVM', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(11, 'Marisol Dickens', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(12, 'Brando Towne I', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(13, 'Evie Bailey', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(14, 'Dolly Halvorson', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(15, 'Prof. Dario Murray', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(16, 'Miss Elda Baumbach', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(17, 'Prof. Consuelo Wintheiser', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(18, 'Dr. Carroll Abbott', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(19, 'Beulah Wolf', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(20, 'Dr. Caesar Johnston', '2016-05-23 12:02:08', '2016-05-23 12:02:08'),
(21, 'June Orn Jr.', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(22, 'Donato Conroy', '2016-05-23 12:02:20', '2016-05-23 12:02:20'),
(23, 'Rickie Heller', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(24, 'Miss Anastasia Lakin', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(25, 'Mr. Jamie Ortiz V', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(26, 'Zachary Haley', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(27, 'Paul Stokes DVM', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(28, 'Kaya Lang', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(29, 'Alphonso Greenfelder I', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(30, 'Nellie Langworth', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(31, 'Hulda Moen', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(32, 'Zoey Stehr', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(33, 'Prof. Bessie Yundt III', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(34, 'Jake Bednar', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(35, 'Mr. Caleb Prohaska DVM', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(36, 'Dr. Earl McDermott Sr.', '2016-05-23 12:02:21', '2016-05-23 12:02:21'),
(37, 'Kyle Stehr', '2016-05-23 12:02:22', '2016-05-23 12:02:22'),
(38, 'Brando Pollich Sr.', '2016-05-23 12:02:22', '2016-05-23 12:02:22'),
(39, 'Prof. Dillan Graham Sr.', '2016-05-23 12:02:22', '2016-05-23 12:02:22'),
(40, 'Dr. Jayme Bode', '2016-05-23 12:02:22', '2016-05-23 12:02:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_shop`
--
ALTER TABLE `product_shop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_shop_product_id_index` (`product_id`),
  ADD KEY `product_shop_shop_id_index` (`shop_id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `product_shop`
--
ALTER TABLE `product_shop`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `product_shop`
--
ALTER TABLE `product_shop`
  ADD CONSTRAINT `product_shop_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_shop_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `shops` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
