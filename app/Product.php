<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded=[];
    public function shops()
    {
        return $this->belongsToMany('App\Shop', 'product_shop' );
    }
}
