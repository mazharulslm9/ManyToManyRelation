<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $guarded=[];
    public function products()
    {
        return $this->belongsToMany('App\Product', 'product_shop');
    }
}
